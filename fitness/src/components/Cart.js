import React from 'react'
import './Cart.css'
import Header from './Header'
import Footer from './Footer'

function Cart() {
  return (
    <>
    <Header />
      <div className='cart-con'>
        <h1>cart</h1>
      </div>
      <Footer />
    </>
  )
}

export default Cart
