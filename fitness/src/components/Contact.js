import React from 'react'
import './Contact.css'
import Footer from './Footer'
import Header from './Header'

function About() {
  return (
    <>
      <Header />
      <div className='contact-container'>
        <div className=''>
          <h1>Contact Us</h1>
          <p>fitness club HQ Office</p>
          <p>#17/17C BDA 3rd Sector,</p>
          <p>Gachibowli,</p>
          <p>Hyderabad,</p>
          <p>Telangana,</p>
          <p>500032</p>
          <p>For general enquiry, please reach out to us on <a href="https://mail.google.com/mail/u/0/#inbox?compose=new">hello@fitnessclub.fit</a></p>
          <p>If you are a health/fitness expert and wish to partner with us, please reach out to us on partner <a href="https://mail.google.com/mail/u/0/#inbox?compose=new">support@fitnessclub.fit</a></p>
          <p>If you wish to join a company as unique as you, please reach out to us on  <a href="https://mail.google.com/mail/u/0/#inbox?compose=new">careers@fitnessclub.com</a></p>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default About
