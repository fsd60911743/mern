import React from 'react'
import './Home.css'
// import { UncontrolledCarousel } from 'reactstrap'
import slide1 from './Assets/images/slide1.jpg';
import slide2 from './Assets/images/slide2.jpg';
import slide3 from './Assets/images/slide3.jpg';
import 'bootstrap/dist/css/bootstrap.min.css';



// function HomeCarousel() {
//   return (
//     <div className='home-container'>
//       <UncontrolledCarousel 
//         items={[
//           {
//             src: slide1,
//             altText: 'Slide 1',
//             header: 'What seems impossible today will one day become your warm-up',
//           },
//           {
//             src: slide2,
//             altText: 'Slide 2',
//             header: 'If you do not make time for exercise, you will probably have to make time for illness.',
//           },
//           {
//             src: slide3,
//             altText: 'Slide 3',
//             header: 'A fitness movement that is worth breaking a sweat for',
//           }
//         ]}
//       />
//     </div>
//   )
// }

// export default HomeCarousel




function HomeCarousel() {
  return (
    <div>
      <div id="carouselExampleCaptions" class="carousel slide home-container" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src={slide1} class="d-block w-100" alt="..."/>
              <div class="carousel-caption d-none d-md-block">
                <h3 className='home-carousel-title'>What seems impossible today will one day become your warm-up</h3>
              </div>
          </div>
          <div class="carousel-item">
            <img src={slide2} class="d-block w-100" alt="..."/>
              <div class="carousel-caption d-none d-md-block">
                <h3 className='home-carousel-title'>If you do not make time for exercise, you will probably have to make time for illness</h3>
              </div>
          </div>
          <div class="carousel-item">
            <img src={slide3} class="d-block w-100" alt="..."/>
              <div class="carousel-caption d-none d-md-block">
                <h3 className='home-carousel-title'>A fitness movement that is worth breaking a sweat for.</h3>
              </div>
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  )
}

export default HomeCarousel
