import React, { useState } from "react" //useEffect,
import './Login.css'
import axios from "axios"
import { useNavigate, Link } from "react-router-dom"
import { FaFacebookSquare, FaYoutube, } from 'react-icons/fa';
import { BsTwitterX } from "react-icons/bs";
import { FaInstagram } from "react-icons/fa";
import { FaLinkedinIn } from "react-icons/fa6";
import poster from './Assets/images/loginposter.png'
// import signupPoster from './Assets/images/signupPoster.png'




function Login() {

    const history = useNavigate();

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    async function submit(e) {
        e.preventDefault();

        try {

            await axios.post("http://localhost:8000/", {
                email, password
            })
                .then(res => {
                    if (res.data === "exist") {
                        history("/home", { state: { id: email } })
                        alert("Login successful")
                    }
                    else if (res.data === "notexist") {
                        alert("User have not signup")
                    }
                })
                .catch(e => {
                    alert("wrong details")
                    console.log(e);
                })

        }
        catch (e) {
            console.log(e);

        }

    }







    return (
        <div className="login-container">
            <div className="login-flex-row">
                <div className="lgoin-background">
                    <h1>Login</h1>
                    <form action="POST" className="login-input">
                        <input className="email" type="email" onChange={(e) => { setEmail(e.target.value) }} placeholder="Email" />
                        <input className="password" type="password" onChange={(e) => { setPassword(e.target.value) }} placeholder="Password" />
                        <input className="submit" type="submit" onClick={submit} />
                    </form>
                    <span className="create-signup">create an account? <Link to="/signup" className="signup-link">Sign UP</Link></span>
                    <div className="login-icons">
                        <h3>Social Media</h3>
                        <a className='login-social-media-icons facebook' href="https://www.facebook.com/" target="blank" ><FaFacebookSquare /></a>
                        <a className='login-social-media-icons youtube' href="https://www.youtube.com/" target="blank"><FaYoutube /></a>
                        <a className='login-social-media-icons twitter' href="https://twitter.com/" target="blank"><BsTwitterX /></a>
                        <a className='login-social-media-icons instagram' href="https://www.instagram.com/" target="blank"><FaInstagram /></a>
                        <a className='login-social-media-icons linkedin' href="https://www.linkedin.com/feed/" target="blank"><FaLinkedinIn /></a>
                    </div>
                </div>
                <img src={poster} alt="poster" className="login-poster" />
            </div>
        </div>
    )
}





export default Login