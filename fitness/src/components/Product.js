import '../App.css'
import React from 'react'
import { useDispatch } from 'react-redux'
import { addtoCart } from '../redux/cartSlice'

function Product({title,image}) {
  return (
    <div>
      <h3>{title}</h3>
      <img src="{image}" alt="" className='w-100' />
      <br />
      <button className='btn btn-warning' onClick={() => dispatch(addtoCart({title,image}))}>
        Add to Cart
      </button>
    </div>
  )
}

export default Product
