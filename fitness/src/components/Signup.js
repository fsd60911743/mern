// import React, {  useState } from "react"  //useEffect,
// import axios from "axios"
// import { useNavigate, Link } from "react-router-dom"


// function Signup() {

//     const history=useNavigate();

//     const [email,setEmail]=useState('')
//     const [password,setPassword]=useState('')

//     async function submit(e){
//         e.preventDefault();

//         try{

//             await axios.post("http://localhost:8000/signup",{
//                 email,password
//             })
//             .then(res=>{
//                 if(res.data ==="exist"){
//                     alert("User already exists")
//                 }
//                 else if(res.data==="notexist"){
//                     history("/login",{state:{id:email}})
//                 }
//             })
//             .catch(e=>{
//                 alert("wrong details")
//                 console.log(e);
//             })

//         }
//         catch(e){
//             console.log(e);

//         }

//     }


//     return (
//         <div className="login">

//             <h1>Signup</h1>

//             <form action="POST">
//                 <input type="email" onChange={(e) => { setEmail(e.target.value) }} placeholder="Email"  />
//                 <input type="password" onChange={(e) => { setPassword(e.target.value) }} placeholder="Password" />
//                 <input type="submit" onClick={submit} />

//             </form>

//             <br />
//             <p>OR</p>
//             <br />

//             <Link to="/">Login Page</Link>

//         </div>





//     )
// }

// export default Signup












import React, { useState } from "react";
import { BiLogoGmail } from "react-icons/bi";
import axios from "axios";
import { useNavigate, Link } from "react-router-dom";
import './Signup.css'
import { FaFacebookSquare, FaYoutube, } from 'react-icons/fa';
import { BsTwitterX } from "react-icons/bs";
import { FaInstagram } from "react-icons/fa";
// import { FaLinkedinIn } from "react-icons/fa6";
import signupPoster from './Assets/images/signupPoster.png'
// import poster from './Assets/images/loginposter.png'



function Signup() {
  const navigate = useNavigate();

  // const [firstname, setfirstname] = useState('');
  // const [lastname, setlastname] = useState('')
  // const [phonenumber, setphonenumber] = useState('')
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post("http://localhost:8000/signup", {
        // firstname,
        // lastname,
        // phonenumber,
        email,
        password
      });

      if (response.data === "exist") {
        alert("User already exists");
      }
      else if (response.data === "notexist") {
        navigate("/", { state: { id: email } });
        alert("Signup successful");
      }
    } catch (error) {
      alert("Something went wrong. Please try again.");
      console.error("Error:", error);
    }
  };













  return (
    <div className="signup-flex-row-container">
      <div className="signup-flex-column-container">
        <div className="signup-input-container">
          <h1 className="signup-heading">Sign Up</h1>
          <form onSubmit={handleSubmit}>
            <div  >
              <div >
                <input
                  className="signup-input-box"
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Email"
                  required
                />
              </div>
              {/* <div >
                <input
                  className="signup-input-box"
                  type="text"
                  value={firstname}
                  onChange={(e) => setfirstname(e.target.value)}
                  placeholder="firstname"
                  required
                />
              </div> */}
              {/* <div >
                <input
                  className="signup-input-box"
                  type="text"
                  value={lastname}
                  onChange={(e) => setlastname(e.target.value)}
                  placeholder="lastname"
                  required
                />
              </div> */}
              {/* <div >
                <input
                  className="signup-input-box"
                  type="number"
                  value={phonenumber}
                  onChange={(e) => setphonenumber(e.target.value)}
                  placeholder="phonenumber"
                  required
                />
              </div> */}
              <div >
                <input
                  className="signup-input-box"
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="Password"
                  required
                />
              </div>
              <div >
                <button type="submit" className="signup-button" >Sign Up</button>
                <div>
                  <span className="sign-up">already have an account?<Link to="/" className="login-link">Login</Link></span>
                </div>
              </div>
            </div>
          </form>
          <div className="signup-icons">
            <h3>Social Media</h3>
            <a className='signup-social-media-icons facebook' href="https://www.facebook.com/" target="blank" ><FaFacebookSquare /></a>
            <a className='signup-social-media-icons youtube' href="https://www.youtube.com/" target="blank"><FaYoutube /></a>
            <a className='signup-social-media-icons twitter' href="https://twitter.com/" target="blank"><BsTwitterX /></a>
            <a className='signup-social-media-icons instagram' href="https://www.instagram.com/" target="blank"><FaInstagram /></a>
            <a className="login-social-media-icons gmail" href="https://mail.google.com/mail/u/0/#inbox?compose=new"><BiLogoGmail /></a>
          </div>
        </div>
        <img src={signupPoster} alt="signup-poster" className="signup-poster" />
      </div>
    </div>



  );
}

export default Signup;