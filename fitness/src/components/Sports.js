import React from 'react'
import './sports.css'
import swim from './Assets/images/swim.jpg'
import badminton from './Assets/images/badminton.jpg'
import table from './Assets/images/table.jpg'
import Header from './Header'
import Footer from './Footer'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { GiShuttlecock } from "react-icons/gi";
import { IoIosPricetags } from "react-icons/io";



function Sports() {
  return (
    <div>
      <Header />
      <div className='sports-container'>
        <div id="carouselExampleCaptions" className="carousel slide" data-bs-ride="carousel">
          <div className="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={swim} className="d-block w-100" alt="..." />
              <div className="carousel-caption d-none d-md-block">
                <h3 className='sports-carousel-title'>No matter how slow you go, you are still lapping everybody on the couch</h3>
              </div>
            </div>
            <div className="carousel-item">
              <img src={badminton} className="d-block w-100" alt="..." />
              <div className="carousel-caption d-none d-md-block">
                <h3 className='sports-carousel-title'>Badminton is not just about winning; it’s about the joy of playing.</h3>
              </div>
            </div>
            <div className="carousel-item">
              <img src={table} className="d-block w-100" alt="..." />
              <div className="carousel-caption d-none d-md-block">
                <h3 className='sports-carousel-title'>Being the best is not good enough. Try harder. Just make sure having fun is a central component of striving for excellence.</h3>
              </div>
            </div>
          </div>
          <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
        <div className='sports-price-plan-programs-card-container' >
          <div className='sports-price-plan-programs-card' >
            <span><FontAwesomeIcon icon="fa-solid fa-person-swimming" size='2xl' />   </span>
            <h4>Swimming  Training</h4>
            <span>This program is suitable for you who wants to get rid of your fat and lose their weight.</span>
            <span>Join Now <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
          </div>
          <div className='sports-price-plan-programs-card'>
            <GiShuttlecock className='sports-icons' />
            <h4>Badminton Training</h4>
            <span>Badminton is an aerobic sport, so focus on building cardiovascular endurance. </span>
            <span>Join Now <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
          </div>
          <div className='sports-price-plan-programs-card'>
            <span>  <FontAwesomeIcon icon="fa-solid fa-table-tennis-paddle-ball" size="2xl" /></span>
            <h4>Table Tennis Training</h4>
            <span>Work on perfecting your strokes, including forehand and backhand drives, loops, chops, and serves.</span>
            <span>Join Now <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
          </div>
        </div>
        <div class="sports-price-plan-programs-header" >
          <span class="home-price-plan-stroke-text">Ready to Start</span>
          <span>Your Journey</span>
          <span class="home-price-plan-stroke-text">now withus</span>
        </div>
        <div className='home-price-basic-plan-container'>

          <div >
            {/* plan - 1 */}
            <div className='home-price-basic-pro-plan'>
              <span><FontAwesomeIcon icon="fa-solid fa-heart-pulse" size='2xl' /></span>
              <h3>BASIC PLAN</h3>
              <h1>₹ 7,990</h1>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />12 Months</span>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Free Training</span>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Access to The Community</span>
              <span data-bs-toggle="modal" data-bs-target="#modal3">See more benefits  <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
              <button class="home-price-join-now-button" data-bs-toggle="modal" data-bs-target="#modal1">Join now</button>
            </div>
            {/* <!-- Button 1 --> */}
            <div class="modal fade" id="modal1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal1Label" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal1Label">Basic Plan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  {/* button - 1 */}
                  <div class="modal-body ">
                    <div className='home-price-tag-container'>
                      <div>
                        <h4>12 Months</h4>
                        <input type="date" />
                      </div>
                      <div className='home-price-tag'>
                        <span className='strike'>price ₹ 14,990</span>
                        <span className='home-price-pop'>After% ₹ 7,990</span>
                        <small className='home-price-pop'>GST   ₹ +1,438</small> <hr />
                        <small >Total ₹ 9,428 </small>
                      </div>
                    </div>
                    <h3>Offer</h3>
                    <p><IoIosPricetags /> Get FREE 1 month extension  </p>
                    <p><IoIosPricetags />Additional Rs.1000 Off applied.  </p>
                    <p> <IoIosPricetags /> 15 Days of membership pause.  </p>
                    <p><IoIosPricetags /> No Cost EMI Available </p>
                    <h3>About this pack</h3>
                    <p>
                      Book unlimited sports sessions anytime at any sports centre in your city for 12
                      months Every  fitness centre offers a premium sports experience and run by highly qualified sport coaches. These sessions are great for
                      newbies and fitness/sports veterans alike, and are guaranteed to show results.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    <a href="https://paytm.com/  " target='blank'><button type="button" class="btn btn-primary">Pay</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div >
            {/* plan - 2 */}
            <div className='home-price-premium-plan'>
              <span><FontAwesomeIcon icon="fa-solid fa-crown" size='2xl' /></span>
              <h3>PREMIUM PLAN</h3>
              <h1>₹ 11,490</h1>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />36 Months</span>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Free training</span>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Access to steam bath</span>
              <span data-bs-toggle="modal" data-bs-target="#modal3">See more benefits  <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
              <button class="home-price-join-now-button" data-bs-toggle="modal" data-bs-target="#modal2">Join now</button>
            </div>
            {/* <!-- Button 2 --> */}
            <div class="modal fade" id="modal2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal2Label" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal2Label">PREMIUM PLAN</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <div className='home-price-tag-container'>
                      <div>
                        <h4>36 Months</h4>
                        <input type="date" />
                      </div>
                      <div className='home-price-tag'>
                        <span className='strike'>price ₹ 24,990</span>
                        <span className='home-price-pop'>After% ₹ 11,490</span>
                        <small className='home-price-pop'>GST   ₹ +2,068</small> <hr />
                        <small >Total ₹ 13,558 </small>
                      </div>
                    </div>
                    <h3>Offer</h3>
                    <p><IoIosPricetags /> Only Today | Get FREE 2 months extension.  </p>
                    <p><IoIosPricetags />Only Today | Additional Rs.2000 Off applied. </p>
                    <p> <IoIosPricetags />45 Days of membership pause.  </p>
                    <p><IoIosPricetags /> No Cost EMI Available.  </p>
                    <h3>About this pack</h3>
                    <p>
                      Book unlimited sports sessions anytime at any sports centre in your city for 36 months
                      Every  fitness centre offers a premium sports experience and run by highly qualified sport coaches. These sessions are great for
                      newbies and fitness/sports veterans alike, and are guaranteed to show results.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    <a href="https://paytm.com/  " target='blank'><button type="button" class="btn btn-primary">Pay</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div >
            {/* plan - 3 */}
            <div className='home-price-basic-pro-plan'>
              <span><FontAwesomeIcon icon="fa-solid fa-dumbbell" size="2xl" /></span>
              <h3>PRO PLAN</h3>
              <h1>₹ 9,490</h1>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />24 Months</span>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' /> consultaion of Private Coaches</span>
              <span><FontAwesomeIcon icon="fa-solid fa-check" size='lg' />Free Fitness Merchandises</span>
              <span data-bs-toggle="modal" data-bs-target="#modal3">See more benefits  <FontAwesomeIcon icon="fa-solid fa-arrow-right" /></span>
              <button class="home-price-join-now-button" data-bs-toggle="modal" data-bs-target="#modal3">Join now</button>

            </div>
            {/* <!-- Button 3 --> */}

            {/* <!-- Modal 3 --> */}
            <div class="modal fade" id="modal3" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal3Label" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal3Label">PRO PLAN</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <div className='home-price-tag-container'>
                      <div>
                        <h4>24 Months</h4>
                        <input type="date" />
                      </div>
                      <div className='home-price-tag'>
                        <span className='strike'>price ₹ 24,990</span>
                        <span className='home-price-pop'>After% ₹ 9,490</span>
                        <small className='home-price-pop'>GST   ₹ +1,708</small> <hr />
                        <small >Total ₹ 11,198 </small>
                      </div>
                    </div>
                    <h3>Offer</h3>
                    <p><IoIosPricetags /> Get FREE 1 month extension  </p>
                    <p><IoIosPricetags />Only Today | Additional Rs.1500 Off applied. </p>
                    <p> <IoIosPricetags /> 30 Days of membership pause  </p>
                    <p><IoIosPricetags /> No Cost EMI Available  </p>
                    <h3>About this pack</h3>
                    <p>
                      Book unlimited sports sessions anytime at any sports centre in your city for
                      24 months Every fitness centre offers a premium sports experience and run by highly qualified sport coaches. These sessions are great for
                      newbies and fitness/sports veterans alike, and are guaranteed to show results.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    <a href="https://paytm.com/  " target='blank'><button type="button" class="btn btn-primary">Pay</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default Sports
